﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damageAmount;
    // Start is called before the first frame update
    void Start()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag != "Projectile")
        {
            Destroy(gameObject);
            Health healthComp = col.gameObject.GetComponent<Health>();
            if (healthComp)
            {
                healthComp.TakeDamage(damageAmount);
            }
        }
    }
}
