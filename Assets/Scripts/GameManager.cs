﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public UnityEvent setupEvent;
    public UnityEvent startLevelEvent;
    public UnityEvent playLevelEvent;
    public UnityEvent endLevelEvent;

    bool m_hasLevelStarted = false;
    bool m_isGamePlaying = false;
    bool m_isGameOver = false;
    bool m_hasLevelFinished = false;
    bool m_isWinner = false;

    public bool HasLevelStarted { get => m_hasLevelStarted; set => m_hasLevelStarted = value; }
    public bool IsGamePlaying { get => m_isGamePlaying; set => m_isGamePlaying = value; }
    public bool IsGameOver { get => m_isGameOver; set => m_isGameOver = value; }
    public bool HasLevelFinished { get => m_hasLevelFinished; set => m_hasLevelFinished = value; }

    public float delay = 1f;

    public GameObject player;

    // Start is called before the first frame update
    void Awake()
    {
    }

    private void Start()
    {
        StartCoroutine("RunGameLoop");
    }

    public void Update()
    {
        if (player)
        {
            Health healtComp = player.GetComponent<Health>();
            if (healtComp)
            {
                float hp = healtComp.healthPointsCurrent;
                Transform canvas = transform.Find("Canvas");
                Text healthText = canvas.Find("HealthText").GetComponent<Text>();
                healthText.text = string.Format("Health: {0}", (int)hp);
            }
        }
    }

    IEnumerator RunGameLoop()
    {
        yield return StartCoroutine("StartLevelRoutine");
        yield return StartCoroutine("PlayLevelRoutine");
        yield return StartCoroutine("EndLevelRoutine");
    }

    IEnumerator StartLevelRoutine()
    {
        Debug.Log("SETUP LEVEL");
        if(setupEvent != null)
        {
            setupEvent.Invoke();
        }

        Debug.Log("START LEVEL");

        while (!m_hasLevelStarted)
        {
            yield return null;
        }

        if(startLevelEvent != null)
        {
            startLevelEvent.Invoke();
        }
    }

    IEnumerator PlayLevelRoutine()
    {
        Debug.Log("PLAY LEVEL");
        m_isGamePlaying = true;
        yield return new WaitForSeconds(delay);

        if (playLevelEvent != null)
        {
            playLevelEvent.Invoke();
        }

        while (!m_isGameOver)
        {
            yield return null;
        }
    }

    IEnumerator EndLevelRoutine()
    {
        Debug.Log("END LEVEL");

        if (endLevelEvent != null)
        {
            endLevelEvent.Invoke();
        }

        while (!m_hasLevelFinished)
        {
            yield return null;
        }

        Transform canvas = transform.Find("Canvas");
        if (m_isWinner)
        {
            canvas.Find("YouWinText").gameObject.SetActive(true);
        }
        else
        {
            canvas.Find("GameOverText").gameObject.SetActive(true);
        }
    }

    void RestartLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void PlayLevel()
    {
        m_hasLevelStarted = true;
    }

    public void GameOver(bool isWinner)
    {
        m_isGameOver = true;
        m_hasLevelFinished = true;
        m_isWinner = isWinner;
    }

}
