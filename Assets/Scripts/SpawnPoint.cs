﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float respawnTime = 5;

    // Start is called before the first frame update
    private GameObject enemy; 
    private float liveTime = 0;
    void Start()
    {
        if(enemyPrefab != null)
        {
            enemy = Instantiate(enemyPrefab, transform.position, transform.rotation);
            liveTime = respawnTime;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(enemy == null)
        {
            liveTime -= Time.deltaTime;
        }

        if (liveTime <= 0)
        {
            liveTime = respawnTime;
            enemy = Instantiate(enemyPrefab, transform.position, transform.rotation);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position, 2);
    }
}
