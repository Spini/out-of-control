﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClampText : MonoBehaviour
{
    public Text textToClamp;

    // Update is called once per frame
    void Update()
    {
        TopDownMovement tdm = gameObject.GetComponentInParent<TopDownMovement>();
        if (tdm)
        {
            Vector3 textPos = tdm.playerCamera.WorldToScreenPoint(this.transform.position);
            textToClamp.transform.position = textPos;
        }
    }
}
