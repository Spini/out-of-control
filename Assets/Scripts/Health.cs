﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float healthPoints;
    public float healthRegenRatio;
    public int healthRegenAmount = 1;
    private float regenLifeTimeCounter = 0;

    public float healthPointsCurrent;

    private void Awake()
    {
        healthPointsCurrent = healthPoints;
    }

    public void Update()
    {
        TopDownMovement player = gameObject.GetComponent<TopDownMovement>();
        if (player)
        {
            if (player.InsideHouse)
            {
                regenLifeTimeCounter += Time.deltaTime;
                if (regenLifeTimeCounter >= healthRegenRatio)
                {
                    if(healthPointsCurrent + healthRegenAmount > healthPoints)
                    {
                        healthPointsCurrent = 20;
                    }
                    else
                    {
                        healthPointsCurrent += healthRegenAmount;
                    }
                    regenLifeTimeCounter = 0;
                }
            }
        }
    }

    public void TakeDamage(float damageAmount)
    {
        healthPointsCurrent -= damageAmount;
        if(healthPointsCurrent <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
        healthPointsCurrent = healthPoints;
    }
}
