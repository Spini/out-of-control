﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public float damageAmount = 1;
    public float grenadeRadius = 5;
    public GameObject grenadePrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag != "Projectile")
        {
            Destroy(gameObject);

            if(grenadePrefab != null)
            {
                GameObject grenadeParticle = Instantiate(grenadePrefab, transform.position, Quaternion.identity);
                //grenadeParticle.GetComponent<ParticleSystem>().Play();
            }

            Collider[] hitColliders = Physics.OverlapSphere(transform.position, grenadeRadius);
            for (int i = 0; i < hitColliders.Length; i++)
            {
                Health healthComp = hitColliders[i].gameObject.GetComponent<Health>();

                if (healthComp && hitColliders[i].gameObject.tag != "Player")
                {
                    healthComp.TakeDamage(damageAmount);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position, grenadeRadius);
    }
}
