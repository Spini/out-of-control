﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    TopDownMovement player;
    Transform roof;
    MeshRenderer roofMesh;
    Transform tettoia;
    Transform paintings;
    List<MeshRenderer> tettoiaList = new List<MeshRenderer>();
    public List<MeshRenderer> paintingsList = new List<MeshRenderer>();
    int numOfPaintings = 0;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<TopDownMovement>().GetComponent<TopDownMovement>();
        roof = gameObject.transform.Find("TopRoof");
        roofMesh = roof.GetComponent<MeshRenderer>();

        tettoia = gameObject.transform.Find("Tettoia");
        tettoia.GetComponentsInChildren<MeshRenderer>();

        MeshRenderer[] tettoiaParts = tettoia.GetComponentsInChildren<MeshRenderer>();
        tettoiaList = new List<MeshRenderer>(tettoiaParts);
        foreach (MeshRenderer tetRend in tettoiaList)
        {
            tetRend.enabled = false;
        }

        paintings = gameObject.transform.Find("Paintings");

        MeshRenderer[] paintingParts = paintings.GetComponentsInChildren<MeshRenderer>();
        paintingsList = new List<MeshRenderer>(paintingParts);
        foreach (MeshRenderer painting in paintingsList)
        {
            painting.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(player.InsideHouse == true && roofMesh.enabled)
        {
            roofMesh.enabled = false;
            foreach(MeshRenderer tetRend in tettoiaList){
                tetRend.enabled = true;
            }
        }
        else if(player.InsideHouse == false && !roofMesh.enabled)
        {
            roofMesh.enabled = true;
            foreach (MeshRenderer tetRend in tettoiaList)
            {
                tetRend.enabled = false;
            }
        }
    }

    public void HangNewPainting()
    {
        paintingsList[numOfPaintings].enabled = true;
        numOfPaintings++;
    }

    private void OnDrawGizmos()
    {
        BoxCollider b = this.GetComponent<BoxCollider>();
        Gizmos.DrawWireCube(this.transform.position, b.size);
    }
}
