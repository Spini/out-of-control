﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsSpawner : MonoBehaviour
{
    public List<GameObject> itemsToTakeHome;
    private int nextItemIndex = 0;

    public void Awake()
    {
        nextItemIndex = 0;
    }

    public GameObject GetNextItem()
    {
        GameObject nextItem = null;
        if (nextItemIndex < itemsToTakeHome.Count) {
            nextItem = itemsToTakeHome[nextItemIndex];
            nextItem.SetActive(true);
            nextItemIndex++;
        }
        return nextItem;
    }

    public void ResetItems()
    {
        foreach (GameObject item in itemsToTakeHome)
        {
            item.SetActive(false);
        }
        nextItemIndex = 0;
    }

    public void SpawnFirstItem()
    {
        ResetItems();
        GetNextItem();
        GameManager gameManager = GetComponent<GameManager>();
        if(gameManager)
        {
            gameManager.PlayLevel();
        }
    }

    public void Update()
    {
        GameObject itemObject = itemsToTakeHome[nextItemIndex - 1];
        if (itemObject.activeSelf == false)
        {
            if(GetNextItem() == null)
            {
                GameManager gameManager = GetComponent<GameManager>();
                if(gameManager)
                {
                    gameManager.GameOver(true);
                }
            }
        }
        else
        {
            Item item = itemObject.GetComponent<Item>();
            if(item)
            {
                if(item.inBackpack)
                {
                    Transform canvas = itemObject.transform.Find("Canvas");
                    canvas.Find("GoText").gameObject.SetActive(false);
                    canvas.Find("ReturnText").gameObject.SetActive(true);
                }
            }
        }
    }
}
