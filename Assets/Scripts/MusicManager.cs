﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource OOCMusic;
    public AudioSource HomeMusic;

    // Start is called before the first frame update
    void Awake()
    {
        HomeMusic.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
