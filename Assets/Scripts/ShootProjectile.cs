﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootProjectile : MonoBehaviour
{
    public float fireRate;
    private float shootTimer;
    public Transform projectileTransformPoint;
    public float projectileForce = 20f;
    public GameObject projectilePrefab;
    // Start is called before the first frame update
    void Awake()
    {
        shootTimer = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        shootTimer += Time.deltaTime;
    }

    public bool Shoot()
    {
        bool shootSuccess = false;
        if (shootTimer > fireRate)
        {
            GameObject projectile = Instantiate(projectilePrefab, projectileTransformPoint.position, projectileTransformPoint.rotation);

            // Add force to the cloned object in the object's forward direction
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            rb.AddForce(projectile.transform.forward * projectileForce);
            Destroy(projectile, 5);
            shootTimer = 0;
            shootSuccess = true;
        }
        return shootSuccess;
    }
}
