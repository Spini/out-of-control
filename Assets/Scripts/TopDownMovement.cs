﻿using UnityEditorInternal;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class TopDownMovement : MonoBehaviour
{
    //Audio stuff
    public MusicManager musicManager;
    //Player Animation Variables
    public Animator animator;
    public Renderer shotgun;
    public Renderer pipe;
    //Player Camera variables
    public enum CameraDirection { x, z }
    public CameraDirection cameraDirection = CameraDirection.x;
    public float cameraHeightOutside = 20f;
    public float cameraHeightInside = 10f;
    public float cameraTiltOutside = 7f;
    public float cameraTiltInside = 4f;
    public Camera playerCamera;
    public GameObject targetIndicatorPrefab;
    //Player Controller variables
    public float maxSpeed = 10.0f;
    public float slowSpeed = 5.0f;
    public float insideHouseSpeed = 2.0f;
    public float gravity = 14.0f;
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    public float fireRate = 2.0f;
    public Transform backpackTransformPoint;
    public float bakcpackAcquisitionRadius;
    //Private variables
    bool grounded = false;
    Rigidbody r;
    GameObject targetObject;
    //Mouse cursor Camera offset effect
    Vector2 playerPosOnScreen;
    Vector2 cursorPosition;
    Vector2 offsetVector = Vector2.zero;
    //Plane that represents imaginary floor that will be used to calculate Aim target position
    Plane surfacePlane = new Plane();
    bool acquisitionButtonPressed = false;
    bool backpackFull = false;
    Item objInBackpack;
    bool insideHouse = false;
    float cameraHeight;
    float cameraTilt;
    float speed;
    public House house;


    public bool InsideHouse { get => insideHouse; set => insideHouse = value; }

    void Awake()
    {
        r = GetComponent<Rigidbody>();
        r.freezeRotation = true;
        r.useGravity = false;

        //Instantiate aim target prefab
        if (targetIndicatorPrefab)
        {
            targetObject = Instantiate(targetIndicatorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        }

        //Hide the cursor
        Cursor.visible = false;
        backpackTransformPoint = gameObject.transform.Find("Backpack");

        cameraHeight = cameraHeightOutside;
        cameraTilt = cameraTiltOutside;

        ItemsSpawner itemSpawner = gameObject.GetComponent<ItemsSpawner>();
        if (itemSpawner)
        {
            itemSpawner.GetNextItem();
        }

        //get Animator
        animator = GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        //Setup camera offset
        Vector3 cameraOffset = Vector3.zero;
        bool isShooting = false;
        if (cameraDirection == CameraDirection.x)
        {
            cameraOffset = new Vector3(cameraTilt, cameraHeight, 0);
        }
        else if (cameraDirection == CameraDirection.z)
        {
            cameraOffset = new Vector3(0, cameraHeight, cameraTilt);
        }

        if (grounded)
        {
            // Calculate how fast we should be moving
            Vector3 targetVelocity = Vector3.zero;
            if (cameraDirection == CameraDirection.x)
            {
                targetVelocity = new Vector3(Input.GetAxis("Vertical") * (cameraTilt >= 0 ? -1 : 1), 0, Input.GetAxis("Horizontal") * (cameraTilt >= 0 ? 1 : -1)).normalized;
            }
            else if (cameraDirection == CameraDirection.z)
            {
                targetVelocity = new Vector3(Input.GetAxis("Horizontal") * (cameraTilt >= 0 ? -1 : 1), 0, Input.GetAxis("Vertical") * (cameraTilt >= 0 ? -1 : 1)).normalized;
            }
            targetVelocity *= speed;

            // Apply a force that attempts to reach our target velocity
            Vector3 velocity = r.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            r.AddForce(velocityChange, ForceMode.VelocityChange);

            // Jump
            if (canJump && Input.GetButton("Jump"))
            {
                r.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }

            if (Input.GetButton("Fire1") && InsideHouse == false)
            {
                Transform gun = transform.Find("GrenadeLauncher");
                if (gun)
                {
                    ShootGrenade shootScript = gun.GetComponent<ShootGrenade>();
                    shootScript.Shoot((GetAimTargetPos() - gun.position).normalized);
                    isShooting = true;
                    transform.LookAt(new Vector3(targetObject.transform.position.x, transform.position.y, targetObject.transform.position.z));
                }
            }
            ManageBackpack();

        }
        // We apply gravity manually for more tuning control
        r.AddForce(new Vector3(0, -gravity * r.mass, 0));

        grounded = false;

        //Mouse cursor offset effect
        //playerPosOnScreen = playerCamera.WorldToViewportPoint(transform.position);
        //cursorPosition = playerCamera.ScreenToViewportPoint(Input.mousePosition);
        //offsetVector = cursorPosition - playerPosOnScreen;

        //Camera follow
        playerCamera.transform.position = Vector3.Lerp(playerCamera.transform.position, transform.position + cameraOffset, Time.deltaTime * 7.4f);
        playerCamera.transform.LookAt(transform.position + new Vector3(-offsetVector.y * 2, 0, offsetVector.x * 2));

        //Aim target position and rotation
        targetObject.transform.position = GetAimTargetPos();
        targetObject.transform.LookAt(new Vector3(transform.position.x, targetObject.transform.position.y, transform.position.z));

        if(isShooting)
        {
            speed = slowSpeed;
        }
        else
        {
            Vector3 dir = Vector3.zero;
            dir.x = Input.GetAxis("Horizontal");
            dir.z = Input.GetAxis("Vertical");
            if (dir != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(-dir);
            }
            if (InsideHouse) {
                speed = insideHouseSpeed;
                shotgun.enabled = false;
                pipe.enabled = true;
            }
            else {
                speed = maxSpeed;
                shotgun.enabled = true;
                pipe.enabled = false;
            }
        }

        //animation shooting
        animator.SetBool("isShooting", isShooting);
        animator.SetFloat("velocity", r.velocity.magnitude);
        animator.SetFloat("VelX", transform.InverseTransformDirection(r.velocity).x);
        animator.SetFloat("VelY", transform.InverseTransformDirection(r.velocity).z);
        animator.SetBool("insideHouse", insideHouse);
    }

    private void ManageBackpack()
    {
        if (Input.GetButton("Fire2") && acquisitionButtonPressed == false)
        {
            acquisitionButtonPressed = true;

            if (backpackFull)
            {
                if (insideHouse)
                {
                    objInBackpack.gameObject.SetActive(false);
                    house.HangNewPainting();
                    backpackFull = false;
                }
            }
            else
            {
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, bakcpackAcquisitionRadius);
                for (int i = 0; i < hitColliders.Length; i++)
                {
                    if (hitColliders[i].GetComponent<Item>() != null)
                    {
                        objInBackpack = hitColliders[i].GetComponent<Item>();
                        backpackFull = true;
                        objInBackpack.inBackpack = true;

                        //objInBackpack.AttackSocket.transform.parent = backpackTransformPoint.transform;
                        objInBackpack.transform.position = backpackTransformPoint.transform.position;
                        objInBackpack.transform.rotation = backpackTransformPoint.transform.rotation;
                        objInBackpack.transform.parent = backpackTransformPoint.transform;
                        break;
                    }
                }
            }

        }
        else if(!Input.GetButton("Fire2") && acquisitionButtonPressed == true)
        {
            acquisitionButtonPressed = false;
        }
    }

    Vector3 GetAimTargetPos()
    {
        //Update surface plane
        surfacePlane.SetNormalAndPosition(Vector3.up, transform.position);

        //Create a ray from the Mouse click position
        Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);

        //Initialise the enter variable
        float enter = 0.0f;

        if (surfacePlane.Raycast(ray, out enter))
        {
            //Get the point that is clicked
            Vector3 hitPoint = ray.GetPoint(enter);

            //Move your cube GameObject to the point where you clicked
            return hitPoint;
        }

        //No raycast hit, hide the aim target by moving it far away
        return new Vector3(-5000, -5000, -5000);
    }

    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, bakcpackAcquisitionRadius);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "House")
        {
            InsideHouse = true;
            cameraHeight = cameraHeightInside;
            cameraTilt = cameraTiltInside;
            targetObject.SetActive(false);

            if (musicManager)
            {
                musicManager.HomeMusic.volume = 1;
                if (musicManager.OOCMusic.isPlaying)
                {
                    musicManager.OOCMusic.volume = 0;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "House")
        {
            InsideHouse = false;
            cameraHeight = cameraHeightOutside;
            cameraTilt = cameraTiltOutside;
            targetObject.SetActive(true);

            if (musicManager)
            {
                musicManager.HomeMusic.volume = 0;
                if (musicManager.OOCMusic.isPlaying)
                {
                    musicManager.OOCMusic.volume = 1;
                }
                else
                {
                    musicManager.OOCMusic.Play();
                    musicManager.OOCMusic.volume = 1;
                }
            }
        }
    }
}